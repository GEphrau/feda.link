<?php

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Helpers\CryptoHelper;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->createAdminUser();
    }

    public function createAdminUser ()
    {
        $hashed_password = Hash::make('fedalink4571');
        $recovery_key = CryptoHelper::generateRandomHex(50);
        $api_active = 1;
        $api_key = CryptoHelper::generateRandomHex(env('_API_KEY_LENGTH'));
        $user = new User;
        $user->username = 'Admin4571';
        $user->password = $hashed_password;
        $user->email = 'admin@feda.link';
        $user->recovery_key = $recovery_key;
        $user->active = 1;
        $user->ip = '127.0.0.1';
        $user->role = 'admin';
        $user->api_key = $api_key;
        $user->api_active = $api_active;

        $user->save();
    }
}
