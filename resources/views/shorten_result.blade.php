@extends('layouts.base')

@section('css')
<link rel='stylesheet' href='/css/shorten_result.css' />
@endsection

@section('content')
<div class="content-div  content-div-padding  ">
    <h3>Shortened URL</h3>
    <div class="input-group">
        <input type="text" class="result-box form-control" value='{{$short_url}}' id="short_url">
        <div class="input-group-addon" id="clipboard-copy" data-clipboard-target="#short_url" data-toggle="tooltip" data-placement="bottom" data-title="Copied!">
            <i class="fa fa-clipboard" aria-hidden="true" title="Copy to clipboard"></i>
        </div>
    </div>
    <a id="generate-qr-code" class="btn btn-primary">Generate QR Code</a>
    <a href="{{route('index')}}" class="btn btn-info">Shorten another</a>
    <div class="qr-code-container" title='{{$short_url}}' style="display: block;">
        <canvas width="280" height="280" style="display: none;"></canvas>
    </div>
</div>
@endsection

@section('js')
<script src="/js/qrcode.min.js"></script>
<script src="/js/clipboard.min.js"></script>
<script src='/js/shorten_result.js'></script>
@endsection
